Readme
======

Some free and open-source software projects use codes of conduct that explicitly coerce their contributors into accepting false religions: [Contributor Covenant](http://www.contributor-covenant.org/), [Mozilla Community Participation Guidelines](https://www.mozilla.org/en-US/about/governance/policies/participation/).
Some other projects try a more humanist approach, talking about respect and other virtues more abstractly: [Debian Code of Conduct](https://www.debian.org/code_of_conduct), [The Ruby Community Conduct Guideline](https://www.ruby-lang.org/en/conduct/).
But both approaches are unable to succeed as it is impossible to harmonize good and evil, as pope Pius IX explains in the encyclical Qui Pluribus:

> Also perverse is the shocking theory that it makes no difference to which religion one belongs, a theory which is greatly at variance even with reason.
> By means of this theory, those crafty men remove all distinction between virtue and vice, truth and error, honorable and vile action.
> They pretend that men can gain eternal salvation by the practice of any religion, as if there could ever be any sharing between justice and iniquity, any collaboration between light and darkness, or any agreement between Christ and Belial.

Having a more abstract code of conduct that talks about virtues without being specific is also not helpful, since falsehood must be replaced with verity, as taught by pope Pius IX in the encyclical Inter Multiplices:

> And here We cannot help but remind you of the admonitions and counsels with which, four years ago, We strongly summoned all the bishops of the Catholic world to exhort men outstanding for talent and sound doctrine to publish appropriate writings with which they might enlighten the minds of people and dissipate the darkness of creeping errors.
> Strive to remove this deadly pestilence of books and magazines from the faithful given into your care.
> At the same time encourage with all benevolence and favor those men who, animated by a Catholic spirit and educated in literature and learning, will endeavor to write books and publish magazines.
> Do this so that the Catholic doctrine is defended and spread, that the venerable rights and documents of this Holy See remain sound, that opinions and doctrines opposed to the same See and its authority may be suppressed, and that the darkness of error is banished and the minds of men illumined with the sweet light of truth.
> And it will be for your episcopal solicitude and love to arouse such inspired Catholic writers, so that they continue with ever greater zeal and knowledge to defend the cause of Catholic truth.
> You must also admonish them like a prudent father if their writings should offend Catholic teaching.

The only way to achieve harmony and peace is through the Church of Our Lord and Savior Jesus Christ, as pope Pius IX show us in the encyclical Maximae Quidem:

> Bishops are also required to continually remind all people that this Church has always been not only the mother and teacher of all virtues, but also the founder and governor of civilization and its benefits, of peace, of progress, and of prosperity.
> She alone is able to preserve the public order which is so gravely disturbed by impiety and rebellion.

Methodology
-----------

The rules and recommendations in this code of conduct come from chapter 4 of The Rules of St. Benedict. Those laws are divided into two groups, those based on natural virtues are rules in the code of conduct, and those based on the theological virtues are only recommendations. This division happens because theological virtues can only result from the grace of God, there being impossible for a person to practice them spontaneously.

Usage
-----

Copy the file CODE\_OF\_CONDUCT.markdown to the root folder of your project, and replace the fields enclosed by brackets "[]" with your project information. (Do not include the brackets.)
